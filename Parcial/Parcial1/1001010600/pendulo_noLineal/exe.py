import PenduloNoLineal as pnl
import numpy as np

if __name__=="__main__":
    '''
    Parametros de entrada:

    l: Longitud del péndulo
    u0: Desplazamiento angular (en grados) inicial del péndulo en t = t0
    v0: Velocidad angular inicial del péndulo en t = t0

    Parametros de solucion de la ecuacion:

    h: Paso de la solución de la ED por medio de RK4
    t: Puntos donde desea hallar la solución

    Metodos:

    Movement: calcula las posiciones y velocidades del péndulo
    Retorna una lista en la cual la primer entrada son las posiciones
    angulares y la segunda entrada son las velocidades angulares.

    angPosition: Grafica la trayectoria
    

    '''
    #Condiciones inciales del péndulo
    l, u0, v0 = 1, 30, 0 
    t0 = 0
    #................................
    h = 0.0001
    t = np.linspace(0,10,100)
    pend = pnl.PendNoLineal(l, u0, v0, t0, h, t)
    pend.angPosition() #Grafica

    '''
    El código tarda buen tiempo en correr, no pude solucionar este problema. Intenté optimizar el código
    vectorizando la funcion de RK4, pero sin obtener un resultado satisfactorio. A pesar del tiempo que
    tarda, el código resuelve el problema y entrega la grafica de la posición angular del péndulo.
    El problema de tiempo creo que se puede solucionar cambiando la condición de parada en el while de RK4,
    pero no sé cómo podría lograr cambiar esta condición y adecuarla para que funcione de manera rápida
    al ingresar un arreglo temporal.
    '''


