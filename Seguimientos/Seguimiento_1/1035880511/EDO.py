import numpy as np
from scipy.integrate import odeint 
import matplotlib.pyplot as plt


"""
En el presente programa se crea una clase con los métodos de Euler y RK4 para la solución de ecuaciones 
diferenciales ordinarias.Inicialmente se pide introducir un diferencial, el rango [a,b] se solución, condiciones 
iniciales, la función a evaluar y el valor de un x para conocer su respectiva coordenada.
Adicionalmente se implementa la solución analítica con ODEINT y con SYMPY
"""
# Creación de la clase "Metodos" que contine las soluciones por Euler y RK4.
class Metodos:
    # Inicialización. 
    def __init__(self,h,a,b,x0,y0,funct,valx): 
        self.h=h
        self.a=a
        self.b=b
        self.x0=x0
        self.y0=y0
        self.funct=funct
        self.valx=valx
    
        
    
    # Se crea el método de Euler
    def meuler(self):
        # Se definen un conjunto de listas para guardar las coordenadas x,y.
        xsol=[]
        ysol=[]
        # Se anexan las condiciones iniciales.
        xsol.append(self.x0)
        ysol.append(self.y0)
        # Se inicializan dos índices i,k para ser usados en el while.
        i = 0
        k = self.a
        # Se implementa un while que permite anexar en la lista x[] y y[] los respectivos valores, teniendo en cuenta el rango ingresado y el incremento
        while self.a <= k <= self.b:
            xsol.append(self.h+xsol[i])
            ysol.append(ysol[i] + self.h*self.funct(xsol[i],ysol[i]))
            i=i+1
            k = k + self.h
        # Se convierten las listas en arreglos para poder buscar la diferencia mínima.
        xsol=np.array(xsol)
        ysol=np.array(ysol)
        # Se crea un nuevo arreglo que contiene la resta xsol-xval.
        dif= np.abs(xsol- self.valx)
        # Se busca el elemento mínimo de este arreglo, el cual representa la coordenada x mas parecida a valx.
        min = np.min(dif)
        # Se obtiene el índice del valor mínimo.
        index=dif.tolist().index(min)
        # Se busca ahora su respectiva coordenada y.
        valy= ysol[index]
        # coordenda deseada
        coord=[self.valx,valy]
        
        return "El conjunto de soluciones Y por el método de Euler es: {}\n La coordenada buscada es{}".format(ysol,coord)
          

    def rk4(self):
        # Se definen un conjunto de listas para guardar las coordenadas x,y.
        xsol=[]
        ysol=[]
        # Se anexan las condiciones iniciales.
        xsol.append(self.x0)
        ysol.append(self.y0)
        # Se inicializan dos índices i,k para ser usados en el while.
        i=0
        k=self.a
        # Se implementa un while que permite anexar en la lista x[] y y[] los respectivos valores, teniendo en cuenta el rango ingresado y el incremento
        while self.a <= k <= self.b:
            k1 = self.h * self.funct(xsol[i],ysol[i])
            k2 = self.h * self.funct(xsol[i]+0.5*self.h , ysol[i]+ 0.5*k1)
            k3 = self.h * self.funct(xsol[i]+0.5*self.h , ysol[i]+ 0.5*k2)
            k4 = self.h * self.funct(xsol[i]+self.h , ysol[i]+k3)

            xsol.append(self.h+xsol[i])
            ysol.append(ysol[i] + (1/6) * (k1+2*k2+2*k3 +k4) )

            i = i+1
            k = k + self.h
        # Se convierten las listas en arreglos para poder buscar la diferencia mínima.
        xsol=np.array(xsol)
        ysol=np.array(ysol)
        # Se crea un nuevo arreglo que contiene la resta xsol-xval.
        dif= np.abs(xsol- self.valx)
        # Se busca el elemento mínimo de este arreglo, el cual representa la coordenada x mas parecida a valx.
        min = np.min(dif)
        # Se obtiene el índice del valor mínimo.
        index=dif.tolist().index(min)
        # Se busca ahora su respectiva coordenada Y.
        valy= ysol[index]
        # coordenda deseada
        coord=[self.valx,valy]
        
        return "El conjunto de soluciones Y por el método RK4 es: {}\n La coordenada buscada es{}".format(ysol,coord)
"""
# Creación de la clase "real" que contiene la solución analítica de la ecuación diferencial
class real:
    # Inicialización.
    def __init__(self,f,y0,a,b,h,valx):
        self.f=f
        self.y0=y0
        self.a=a
        self.b=b
        self.h=h
        self.valx=valx
        
    # Se implementa el método de solución analítico con ODEINT.
    def ode(self):
        # Se define el número de intervalos y se crea un arreglo para la variable independiente.
        n=(self.b-self.a)/self.h
        x = np.linspace(self.a,self.b,int(n))
        y = odeint(self.f,self.y0,x)
        # Se asegura tener los arreglos para x y y.
        x=np.array(x)
        y=np.array(y)
        # Se crea un nuevo arreglo que contiene la resta xsol-xval.
        dif= np.abs(x- self.valx)
        # Se busca el elemento mínimo de este arreglo, el cual representa la coordenada x mas parecida a valx.
        min = np.min(dif)
        # Se obtiene el índice del valor minimo.
        index=dif.tolist().index(min)
        # Se busca ahora su respectiva coordenada Y.
        valy= y[index]
        # Coordenda deseada.
        coord=[self.valx,valy[0]] 
        # Se implementa un retorno de manera gráfica.
        
    
        fig=plt.figure()
        plt.plot(x,y)
        plt.plot(self.valx,valy[0],'o')
        plt.title("Solución por ODEINT")
        plt.show()
        return fig
        return "El conjunto de soluciones Y por el método analítico ODEINT es: {}\n La coordenada buscada es{}".format(y,coord)
        """

# Nota: la implementación de manera analítica por medio de la función ODEINT presentó errores en los resultados 
#       entregados, sin embargo se muestra la implementación del método.

        

        





        
        

   


        