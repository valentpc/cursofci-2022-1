import numpy as np 


#Definamos la clase para resolver EDO con el método de Euler.
class euler:
    def __init__(self,f,a,b,n,y_0):
        self.a=a
        self.b=b
        self.n=n
        self.y_0=y_0
        self.f=f 


    def p_i(self):
        x = np.linspace(self.a, self.b, self.n+1)
        y = np.zeros(self.n+1)
        y[0] = self.y_0
        h = (self.b- self.a)/self.n  #tamaño de paso
        for i in range(1, self.n+1):
            y[i] = y[i-1] + h * self.f(x[i-1], y[i-1])
        return np.around(y[-1],3)
        
#Definamos la clase para resolver EDO con el método Runge Kutta 4.   
class rk4():
    def __init__(self,f,b,a,n,y_0):
        self.a=a
        self.b=b
        self.n=n
        self.y_0=y_0
        self.f=f 

    def p_ii(self):
        x = np.linspace(self.a, self.b, self.n+1)
        y = np.zeros(self.n+1)
        k1 = np.zeros(self.n+1)
        k2 = np.zeros(self.n+1)
        k3 = np.zeros(self.n+1)
        k4 = np.zeros(self.n+1)



        y[0] = self.a
        h = (self.b - self.a)/self.n
        for i in range(1, self.n+1):
            k1 = h*self.f(x[i-1] , y[i-1])
            k2 = h*self.f(x[i-1] + h/2 , y[i-1] + h*k1/2)
            k3 = h*self.f(x[i-1]+h/2,y[i-1]+k2/2)
            k4 = h*self.f(x[i-1]+h,y[i-1]+h)

            y[i] = y[i-1] + (k1 + k2+k3+k4)/6.
        return y[-1]    

class analitica:
    def __init__(self,f,b,a,n,y_0):
        self.a=a
        self.b=b
        self.n=n
        self.y_0=y_0
        self.f=f 
    #Me fue imposible instalar Scypy :(

            







