
import numpy as np
import matplotlib.pyplot as plt 
from mpl_toolkits.mplot3d import axes3d


class trayectorias:
    
    #Método constructor

    def __init__(self,theta,alpha,B,m,EK,t):
        self.theta=theta
        self.alpha=alpha
        self.B=B
        self.m=m
        self.EK=EK
        self.t=t
    t=np.arange(0,100,1000)
    #Encontremos la magnitud de la velocidad
    
    def mvel(self):
        magnvel=np.sqrt((2*1.6e-19*self.EK)/9.11e-31)
        return magnvel

        #Ahora veamos las componentes de esa velocidad
    
    def cx(self):
        compx=self.mvel()*np.sin(self.theta)*np.cos(self.alpha)
        return compx

    def cy(self):
        compy=self.mvel()*np.sin(self.theta)*np.sin(self.alpha)
        return compy

    def cz(self):
        compz=self.mvel()*np.cos(self.theta)
        return compz

    #Obtengamos las posiciones

    def posx(self):
        px=(self.cx()/((1.6e-19*self.B)/self.m))*np.sin(((1.6e-19*self.B)/self.m)*self.t)
        return px

    def posy(self):
        py=(self.cy()/((1.6e-19*self.B)/self.m))*np.cos(((1.6e-19*self.B)/self.m)*self.t)
        return py
    def posz(self):
        pz=self.cz()*self.t
        return pz

    


























