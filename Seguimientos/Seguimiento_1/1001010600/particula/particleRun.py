import particle as pt

if __name__ == '__main__':
    '''
    Parametros

    q: Carga del electron
    m: Masa del electron
    B: Campo magnetico
    ang: Angulo en grados de la velocidad respecto al campo B
    E: Energia cinetica de la particula en eV

    ========================================================

    Metodos de la clase movement

    xTrayectory: Proporciona la grafica de la trayectoria en x
    yTrayectory: Proporciona la grafica de la trayectoria en y
    zTrayectory: Proporciona la grafica de la trayectoria en z
    _3dTrayectory: Proporciona la grafica de la trayectoria en 3 dimensiones

    x, y o z: Proporciona los valores de de la posición en las diferentes componentes

    '''
    q, m, E, B, ang = 1.602e-19, 9.109e-31, 18.6, 600e-6, 30

    #Pasamos los parametros a la clase para acceder a sus metodos
    Temporary = pt.movement(q, m, E, B, ang)

    #Obtenemos los diferentes graficos

    #Temporary.xTrayectory()
    #Temporary.yTrayectory()
    #Temporary.zTrayectory()
    Temporary._3dTrayectory()

    '''
    Prueba del codigo.

    Probando para varios valores del campo magnetico B, se puede ver que el grafico en 3D da una cosa extraña.
    Buscando una solución, encontré que con un campo magnetico grande el grafico extraño se puede mejorar 
    usando una mayor cantidad de puntos a la hora de hacer el plot (se puede disminuir el paso en el t definido
    en particle.py). La raiz de este problema puede caer en que a medida que el campo B crece, las posiciones en
    x e y tienden a 0.
    Para que el usuario pueda tener más control sobre este problema, se puede pasar el array de t como parametro
    para que por testeo pueda ver que paso en el array funciona para dar un mejor grafico
    '''





    