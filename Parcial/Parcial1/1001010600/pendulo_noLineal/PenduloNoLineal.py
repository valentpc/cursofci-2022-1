import numpy as np
import matplotlib.pyplot as plt

class PendNoLineal:
    def __init__(self, l, u0, v0, t0, h, t):
    #Inicializamos los parametros
        self.l = l
        self.u0 = u0*(np.pi/180)
        self.v0 = v0
        self.t0 = t0
        self.h = h
        self.x = t

    def RK4(self, t):
        '''
        El metodo recibe como parametros
        x: Es el eje temporal
        y: condiciones iniciales para u' y u''
        '''

        def fun(x, y):
            u, p = y[0], y[1]
            return np.asarray([p, -(9.8/ self.l) * np.sin(u)])
        '''
        Con un cambio de variable, se soluciona la u' y u'' al mismo tiempo usando matrices.
        '''

        x = self.t0
        y = np.asarray([self.u0, self.v0])
        #Funciones de Runge Kuta
        def K1(x, y):
            return self.h * fun(x, y)

        def K2(x, y):
            return self.h * fun(x+0.5*self.h, y + 0.5*K1(x, y))

        def K3(x, y):
            return self.h * fun(x+0.5*self.h, y + 0.5*K2(x, y))

        def K4(x, y):
            return self.h * fun(x + self.h, y + K3(x, y))
        #Calculo de yn+1
        while abs(x-t)>0.0001:
            y = y + (1/6)*( K1(x, y) + 2*K2(x, y) + 2*K3(x, y) + K4(x, y))
            x = self.h + x
        return y
    
    def movement(self):
        angPos = []
        vel = []
        for i in range(0, len(self.x)):
            temp = self.RK4(self.x[i])
            angPos.append(temp[0])
            vel.append(temp[1])
        return angPos, vel
        '''
        temp arroja las soluciones para la posicion y la velocidad del pendulo, calculadas con RK4
        luego se almacenan en dos listas diferentes y se retornan los resultados como una tupla
        '''
    
    def angPosition(self):
        
        plt.plot(self.x, self.movement()[0], color = 'crimson')
        plt.ylabel("u [rad]")
        plt.xlabel("t [s]")
        plt.title("Posición angular en función del tiempo")
        plt.show()
