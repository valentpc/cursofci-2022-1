import numpy as np

class PenduloNoLineal:
    
    def __init__(self, t0, u0, w0, l, t):
        
        # Condiciones iniciales del pendulo simple
        self.t0 = t0    # Tiempo inicial
        self.u0 = u0 * np.pi / 180    # Angulo incial 
        self.w0 = w0    # Velocidad angular inicial
        self.g = 9.8   # Gravedad en unidades [m / s*s]
        self.l = l      # Longitud del pendulo
        self.t = t      # Tiempo hasta donde se quiere calcular las coordenadas

    # Metodo para desacoplar en dos funciones la ecuacion diferencial no lineal de 2 orden del pendulo
    def EcuacionPenduloSimple(self, t, y):

        # Como el pendulo simple se rige a través de una ecuación de segundo orden se hace
        # un desacople de este en dos ecuaciones de primer orden haciendo un cambio de variable
        # du/dt = w  donde u: es el desplazamiento angular y así
        # dw/dt = -g/l * sin(u)

        # Primer funcion: du/dt = w
        f1 = y[1]     
        # Segunda funcion: dw/dt = -g/l * sin(u)
        f2 = -self.g / self.l * np.sin(y[0])

        return np.array([f1, f2])

    # Metodo de Runge-Kutta de 4 orden
    def rk4(self, h):
        # h: Intervalo de muestreo

        # Array con las condiciones iniciales
        y = np.array([self.u0, self.w0])
        # Numero de pasos 
        n = round((self.t - self.t0) / h)
        # Numero de ecuaciones desacopladas
        ne = len(y)
        # Inicializar los runge-kuttas con arrays 0, para evaluarlas en cada ecuación 
        k1 = k2 = k3 = k4 = np.zeros(ne)
        # Array de tiempo
        tiempo = np.zeros(n)
        # Array de ceros para ingresar las coordenadas (soluciones aproximadas) para cada ecuación
        Y = np.zeros((ne, n))
        # Tiempo inicial
        ti = self.t0
        # Contador
        i = 0

        for _ in np.arange(self.t0, self.t, h):

            # En cada columna ingresar las aproximaciones de u y w en un instante ti
            Y[:, i] = y
            # Ingresar los diferentes tiempos donde se calculan las aproximaciones
            tiempo[i] = ti

            # Algoritmo de Runge-Kutta de 4 orden 
            k1 = h * self.EcuacionPenduloSimple(ti, y)
            k2 = h * self.EcuacionPenduloSimple(ti + 1 / 2 * h, y + 1 / 2 * k1)
            k3 = h * self.EcuacionPenduloSimple(ti + 1 / 2 * h, y + 1 / 2 * k2)
            k4 = h * self.EcuacionPenduloSimple(ti + h, y + k3)

            # Aproximacion de u y w en ti
            y = y + 1 / 6 * (k1 + 2 * k2 + 2 * k3 + k4)

            # AUmento del tiempo
            ti += h
            
            i += 1

        return Y, tiempo

    # Metodo para obtener la aproximacion del desplazamiento angular
    def DesplazamientoAngular(self, h):
        
        # Obteniendo las aprxoimaciones de u y w
        Y, t = self.rk4(h)
        # Desplazamiento angular
        u = Y[0]

        return u, t
    


